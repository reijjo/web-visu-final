/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        oma1: "#C34C25",
        oma11: "hsl(15, 68%, 35%)",
        oma2: "#FDDA7D",
        oma22: "hsl(44, 97%, 84%)",
        oma23: "hsl(44, 97%, 64%)",

        overwhite: "rgba(255, 255, 255, 0.5)",
        overblack: "rgba(0, 0, 0, 0.5)",
        omaover2: "rgba(253, 218, 125, 0.9)",

        mobile: "#DEB1A1",
        tablet: "#ECE2E4",
        desktop: "#EBD0BC",
      },
    },
    screens: {
      md: "481px",
      lg: "769px",
    },
  },
  plugins: [],
};
