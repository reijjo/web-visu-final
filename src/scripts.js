function openNavMenu() {
  var navMenu = document.getElementById("navMenu");
  var overlay = document.getElementById("overlay");
  var cancelIcon = document.getElementById("cancel-icon");

  navMenu.style.transform = "translateX(0)";

  overlay.style.zIndex = "10";
  overlay.style.backgroundColor = "var(--overblack)";

  cancelIcon.style.transition = "transform 0.5s ease-out";

  setTimeout(function () {
    cancelIcon.style.transform = "rotate(-90deg)";
  }, 200);
}

function closeNavMenu() {
  var navMenu = document.getElementById("navMenu");
  var overlay = document.getElementById("overlay");
  var cancelIcon = document.getElementById("cancel-icon");

  navMenu.style.transform = "translateX(-100%)";

  overlay.style.zIndex = "-10";
  overlay.style.backgroundColor = "transparent";

  cancelIcon.style.transition = "transform 0.5s ease-out";
  cancelIcon.style.transform = "rotate(90deg)";
}
