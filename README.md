# web-visu-final

## Huge Lightspeed Co.

Try it live <a href="https://web-visu-final-reijjo-6893baeb30d828570d668907187428263a9fbb099.gitlab.io">here</a>!

### Final Assignment of Jyväskylä AMK Web-visualisointi course

I got the subject randomly generated from <a href='https://goodbrief.io/'>goodbrief.io</a>

<details>
<summary>Show subject</summary>
<h4>Company Name:</h4>
<div>Huge Lightspeed Co.</div>

<h4>Company Description:</h4>
<div>We are a family store that sells comic-books. Our main product stands out because of its reputation and polish. Our target audience is men. We want to convey a sense of delight, while at the same time being kind.</div>

<h4>Job Description:</h4>
<div>You must create a website that will mainly sell the company's products. The goal is to make the website easy to navigate. Besides the landing page, the website will need a contact page, product pages and a terms of service page. The landing page should have a About the Team section. There should be a call to action to get users to subscribe to the newsletter. They would prefer a minimal design, and would like you to use the brand color, which is red. Take into account the client's preferences and values.</div>

- A really raw Figma preview for the project <a href="https://www.figma.com/proto/nH6jlFaPagWmHR2tjzecrx/Untitled?page-id=0%3A1&type=design&node-id=1-5&viewport=861%2C577%2C0.49&t=7s3TXFII3dsNR7Nl-1&scaling=scale-down&starting-point-node-id=1%3A5&mode=design" target="_blank">here</a>

</details>

### Done with

- HTML
- TailwindCSS

#### Other

- Images and texts from Microsoft Copilot
- Deployed with GitLab Pages
